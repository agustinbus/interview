-- DATABASE DUMP:
-- https://drive.google.com/file/d/1X2QgEpmvc8PoHHJ12mxfwAOldMuxeKjj/view?usp=sharing

-- 1) Top 5 Customers from # of orders.
SELECT
    COUNT(o.orderNumber) as ordersCount,
    c.*
FROM orders as o
LEFT JOIN customers as c on c.`customerNumber` = o.`customerNumber`
GROUP BY c.customerNumber
ORDER BY ordersCount DESC
LIMIT 5;

-- 2) Monthly Sales for the Year 2004
SELECT
	CONCAT(DATE_FORMAT(o.orderDate, '%Y'), ', ',
    DATE_FORMAT(o.orderDate, '%M' )) AS month,
    SUM(od.priceEach * od.quantityOrdered) AS sales
FROM orders AS o
LEFT JOIN orderdetails AS od ON od.orderNumber = o.orderNumber
WHERE YEAR(o.orderDate) = '2004'
GROUP BY CONCAT(DATE_FORMAT(o.orderDate, '%Y'), ', ' ,DATE_FORMAT(o.orderDate, '%M' )) ;

-- 3) Products that sold more than 5 times within the same month
SELECT
	p.productCode,
	COUNT(od.productCode) AS qty,
    DATE_FORMAT(o.orderDate, '%M') AS month

FROM orders AS o
LEFT JOIN orderdetails AS od ON od.orderNumber = o.orderNumber
LEFT JOIN products AS p ON p.productCode = od.productCode
GROUP BY p.productCode, DATE_FORMAT(o.orderDate, '%M')
HAVING qty > 5;
